<?php
use Illuminate\Support\Facades\Input;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// trang chu
Route::get('/', function () {
    return view('pages/trangchu');
});

// test ajax
Route::get('/ajax', function () {
    return view('livesearch');
});

Route::get('/livesearch/action','MyController@action')->name('livesearch.action');

Route::post('login', ['as'=>'login', 'uses'=>'Auth\AuthController@login']);
Route::post('signup', ['as'=>'signup', 'uses'=>'Auth\AuthController@signup']);


Route::get('logout', ['as'=>'logout', 'uses'=>'Auth\AuthController@logout']);

Route::get('trang-chu', [
    'as'=>'trangchu',
    'uses'=>'PageController@getHomePage' 
]);

Route::get('danh-sach-san-pham/{id_type1}', [
    'as'=>'danhsachsp',
    'uses'=>'PageController@getProductList'
]);

Route::get('chi-tiet-san-pham/{id_product}', [
    'as'=>'chitietsp',
    'uses'=>'PageController@getProductDetail'
]);

Route::post('/test', function (Request $request) {
    // test post
    echo Input::get('Email');
    echo Input::get('Password');
});

Route::get('/test', function (Request $request) {
    // test post
    echo Input::get('Email');
    echo Input::get('Password');

   
});
// Route::post('/cart', 'CartController@cart');
Route::get('/cart', 'CartController@cart');

Route::get('/cart/removeitem', 'CartController@removeitem');
Route::get('/cart/additem', 'CartController@addtocart');


/* test chuc nang, html css
Route::get('/laravel', function () {
    return view('pages/laravel');
});
*/







/* Đoạn dưới dùng test Laravel


Route::get('KhoaHoc', function () {
    return view('goodjob');
});

Route::get('TestController', 'MyController@XinChao');


Route::get('TestController-{test}', 'MyController@KhoaHoc');

Route::get('Route1', ['as'=>'MyRoute1', function(){
    echo "Vào Route1 thành công....";
}]);

Route::prefix('poppy')->group(function(){
    Route::get('user1', function(){
        echo "user1 func 1";
    });

    Route::get('user2/profile', function(){
        echo "user2 func 2";
    });
});

Route::get('MyRequest','MyController@GetURL');

Route::get('getForm', function(){
    return view('postForm');
});

Route::post('postForm', ['as'=>'postForm','uses'=>'MyController@postForm']);

Route::get('setCookie', 'MyController@setCookie');
Route::get('getCookie', 'MyController@getCookie');

Route::get('uploadFile', function(){
    return view('postfile');
});

Route::post('postfile', ['as'=>'postfile','uses'=>'MyController@postFile']);

Route::get('getJson', 'MyController@getJson');

Route::get('myView', 'MyController@myView');

//truyen tham so view
Route::get('myView/{ten}', 'MyController@myViewThamSo');

Route::get('blade/{var}', function($var){
    if ($var == "laravel")
        return view('pages.laravel');
    elseif ($var == "php")
        return view('pages.php', [
            'IBN'=>'<i>tham so them vao</i>',
            'IBN5100' => '<strong>Strong BOW</strong>'
        ]);
});

//Database
Route::get('taobang', function(){
    Schema::create('SanPhamXX', function($table){
        $table->increments('id');
        $table->string('ten',200)->nullable();
        $table->string('nsx',200)->default('Kim Dong');
    });

    echo "Đã thực hiện lệnh tạo bảng.";
});

//tao bang lien ket
Route::get('taobanglienket', function(){
    Schema::create('banglienket', function($table){
        $table->increments('id');
        $table->string('ten',200)->nullable();
        $table->integer('idSanPhamXX')->unsigned();
        $table->foreign('idSanPhamXX')->references('id')->on('SanPhamXX');
    });

    echo "Đã thực hiện lệnh tạo bảng liên kết.";
});


//Them cot
Route::get('themcot', function(){
    Schema::table('banglienket', function($table){
        $table->integer('soluong');
    });

    echo "Đã thực hiện lệnh thêm cột.";
});

Route::get('qb/getdata', function(){
    $data = DB::table('sanpham')->where('id','>=',2)->orderBy('id','desc')->count();
    echo $data;
    // foreach($data as $row){
    //     foreach($row as $key=>$value){
    //         echo $key. ":" . $value . "<br>";
    //     }
    //     echo "<hr>";
    // }
});

Route::get('model/saveUser', function(){
    // $user = new App\User();

    // $user->name = "Trang";
    // $user->email = "Trang@gmail.com";
    // $user->password = bcrypt("matkhau");

    // $user->save();

    echo "Da save<br>";


    // $getuser = App\User::find(1);
    // echo $getuser->email;

    // test paginate : phan trang

    echo "<style>.pagination li{list-style:none;float:left;margin-left:5px;</style>";

    $sanpham =App\sanpham::where('id','>=','3')->paginate(3);
    foreach($sanpham as $value){
        echo $value->id . " ".$value->ten . "<br>";
    }

    echo $sanpham->links();

});


Route::get('model/sanpham/save/{ten}', function($ten){
    // $sanpham = new App\sanpham();

    // $sanpham->ten = $ten;
    // $sanpham->soluong = 999;
    
    // $sanpham->save();

    var_dump(App\sanpham::all()->toArray());

});

Route::get('middleware/diem', function(){
    echo "ban da co diem";
})->middleware('MyMiddle');

Route::get('middleware/loi', function(){
    echo "loi! chua co diem";
})->name('middleware/loi');

Route::get('dangnhap',function(){
    return view('dangnhap');
});

Route::post('login', ['as'=>'login','uses'=>'Auth\AuthController@login']);

Route::get('logout', 'Auth\AuthController@logout');

Route::get('thu',function(){
    return view('thanhcong');
});

Route::group(['middleware' => 'web'],function(){
    Route::get('Session', function(){
        Session::put('TenAnhMayLa', 'HungQuoc Nha');
        Session::flash('flashtest', 'hoc toan');
        echo "Đã đặt session";

        echo Session::get('TenAnhMayLa');
        echo Session::get('flashtest');

        // echo Session::has('TenAnhMayLa');
    })  ;
    Route::get('Session/flash', function(){
        echo Session::get('TenAnhMayLa');
        echo "<br>";
        echo Session::get('flashtest');
    })  ;
});

*/
