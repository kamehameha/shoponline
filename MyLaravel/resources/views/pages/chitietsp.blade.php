@extends('layouts.master')

@section('content')

<div class="inner-header">
	<div class="container">
		<div class="pull-left">
			<div class="beta-breadcrumb font-large">
				<a href="{{Route('trangchu')}}">Trang chủ</a> / <span>Thông tin chi tiết sản phẩm</span>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<style>
	.checked {
		color: orange;
	}
</style>
<div class="container">
	<div id="content">
		<div class="row">
			<div class="col-sm-9">

				<div class="row">
					<div class="col-sm-4">
						<img src="../{{$sanpham->HinhSP}}" alt="">
					</div>
					<div class="col-sm-8">
						<div class="single-item-body">
							<p class="single-item-title" style="font-size: 26px; font-family: Time New Roman">{{$sanpham->TenSP}}</p>
							<p class="single-item-price" style="margin-top: 10px">
								<span>{{number_format($sanpham->GiaNiemYet, 0, ',', '.')}}đ</span>
							</p>
						</div>

						<div class="clearfix"></div>

						<div id="rateBox" style="margin-top: 20px"></div>
						<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
						</script>
						<script src="{{URL::asset('js/rate.js')}}"></script>
						<script>
							$("#rateBox").rate({
								length: 5,
								value: {
									{
										$sanpham - > DiemTB
									}
								},
								half: true,
								decimal: false,
								readonly: true,
								text: true,
								textList: ['{{$sanpham->DiemTB}}/5'],
								size: '20px',
								theme: 'orange',
								gutter: '1px',
								selectClass: 'fxss_rate_select',
								incompleteClass: 'fxss_rate_no_all_select',
								customClass: 'custom_class',
								callback: function(object) {
									console.log(object)
								}
							});
						</script>

						<div class="space20">&nbsp;</div>

						<p>Options:</p>
						<div class="single-item-options">
							@if (isset($dsloaisp))
							<select class="wc-select" name="product_type">
								@FOREACH ($dsloaisp as $loaisp)
								<option value="{{$loaisp->IDLSP}}">{{$loaisp->TenLoaiSP}}</option>
								@ENDFOREACH
							</select>
							@else
							<select class="wc-select" name="product_type">
								<option>No product</option>
							</select>
							@endif
							<!-- <select class="wc-select" name="color">
									<option>Color</option>
									<option value="Red">Red</option>
									<option value="Green">Green</option>
									<option value="Yellow">Yellow</option>
									<option value="Black">Black</option>
									<option value="White">White</option>
								</select> -->
							<select class="wc-select" name="quantity">
								<!-- <option>Qty</option> -->
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
							</select>
							<a class="add-to-cart" href="#"><i class="fa fa-shopping-cart"></i></a>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>

				<div class="space40">&nbsp;</div>
				<div class="woocommerce-tabs">
					<ul class="tabs">
						<li><a href="#tab-description">Mô tả sản phẩm</a></li>
						<li><a href="#tab-reviews">Đánh giá (0)</a></li>
					</ul>

					<div class="panel" id="tab-description">
						<p>{{$sanpham->MoTaSP}}</p>
						<p></p>
					</div>
					<div class="panel" id="tab-reviews">
						@FOREACH ($danhgiasp as $dg)
						<p>{{$dg->BinhLuan}}</p>
						@ENDFOREACH
					</div>
				</div>
				<div class="space50">&nbsp;</div>
				<div class="beta-products-list">
					<h4>Related Products</h4>

					<div class="row">
						<div class="col-sm-4">
							<div class="single-item">
								<div class="single-item-header">
									<a href="product.html"><img src="source/assets/dest/images/products/4.jpg" alt=""></a>
								</div>
								<div class="single-item-body">
									<p class="single-item-title">Sample Woman Top</p>
									<p class="single-item-price">
										<span>$34.55</span>
									</p>
								</div>
								<div class="single-item-caption">
									<a class="add-to-cart pull-left" href="product.html"><i class="fa fa-shopping-cart"></i></a>
									<a class="beta-btn primary" href="product.html">Details <i class="fa fa-chevron-right"></i></a>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="single-item">
								<div class="single-item-header">
									<a href="product.html"><img src="source/assets/dest/images/products/5.jpg" alt=""></a>
								</div>
								<div class="single-item-body">
									<p class="single-item-title">Sample Woman Top</p>
									<p class="single-item-price">
										<span>$34.55</span>
									</p>
								</div>
								<div class="single-item-caption">
									<a class="add-to-cart pull-left" href="product.html"><i class="fa fa-shopping-cart"></i></a>
									<a class="beta-btn primary" href="product.html">Details <i class="fa fa-chevron-right"></i></a>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="single-item">
								<div class="ribbon-wrapper">
									<div class="ribbon sale">Sale</div>
								</div>

								<div class="single-item-header">
									<a href="#"><img src="source/assets/dest/images/products/6.jpg" alt=""></a>
								</div>
								<div class="single-item-body">
									<p class="single-item-title">Sample Woman Top</p>
									<p class="single-item-price">
										<span class="flash-del">$34.55</span>
										<span class="flash-sale">$33.55</span>
									</p>
								</div>
								<div class="single-item-caption">
									<a class="add-to-cart pull-left" href="#"><i class="fa fa-shopping-cart"></i></a>
									<a class="beta-btn primary" href="#">Details <i class="fa fa-chevron-right"></i></a>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
					</div>
				</div> <!-- .beta-products-list -->
			</div>
			<div class="col-sm-3 aside">
				<div class="widget">
					<h3 class="widget-title">Best Sellers</h3>
					<div class="widget-body">
						<div class="beta-sales beta-lists">
							<div class="media beta-sales-item">
								<a class="pull-left" href="product.html"><img src="source/assets/dest/images/products/sales/1.png" alt=""></a>
								<div class="media-body">
									Sample Woman Top
									<span class="beta-sales-price">$34.55</span>
								</div>
							</div>
							<div class="media beta-sales-item">
								<a class="pull-left" href="product.html"><img src="source/assets/dest/images/products/sales/2.png" alt=""></a>
								<div class="media-body">
									Sample Woman Top
									<span class="beta-sales-price">$34.55</span>
								</div>
							</div>
							<div class="media beta-sales-item">
								<a class="pull-left" href="product.html"><img src="source/assets/dest/images/products/sales/3.png" alt=""></a>
								<div class="media-body">
									Sample Woman Top
									<span class="beta-sales-price">$34.55</span>
								</div>
							</div>
							<div class="media beta-sales-item">
								<a class="pull-left" href="product.html"><img src="source/assets/dest/images/products/sales/4.png" alt=""></a>
								<div class="media-body">
									Sample Woman Top
									<span class="beta-sales-price">$34.55</span>
								</div>
							</div>
						</div>
					</div>
				</div> <!-- best sellers widget -->
				<div class="widget">
					<h3 class="widget-title">New Products</h3>
					<div class="widget-body">
						<div class="beta-sales beta-lists">
							<div class="media beta-sales-item">
								<a class="pull-left" href="product.html"><img src="source/assets/dest/images/products/sales/1.png" alt=""></a>
								<div class="media-body">
									Sample Woman Top
									<span class="beta-sales-price">$34.55</span>
								</div>
							</div>
							<div class="media beta-sales-item">
								<a class="pull-left" href="product.html"><img src="source/assets/dest/images/products/sales/2.png" alt=""></a>
								<div class="media-body">
									Sample Woman Top
									<span class="beta-sales-price">$34.55</span>
								</div>
							</div>
							<div class="media beta-sales-item">
								<a class="pull-left" href="product.html"><img src="source/assets/dest/images/products/sales/3.png" alt=""></a>
								<div class="media-body">
									Sample Woman Top
									<span class="beta-sales-price">$34.55</span>
								</div>
							</div>
							<div class="media beta-sales-item">
								<a class="pull-left" href="product.html"><img src="source/assets/dest/images/products/sales/4.png" alt=""></a>
								<div class="media-body">
									Sample Woman Top
									<span class="beta-sales-price">$34.55</span>
								</div>
							</div>
						</div>
					</div>
				</div> <!-- best sellers widget -->
			</div>
		</div>
	</div> <!-- #content -->
</div> <!-- .container -->


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<Script>
	//  su dung ajax  cai dat nut  them vao gio hang
	$(".add-to-cart").click(function(e) {
		e.preventDefault();
		// lay loai san pham
		var loaisp = $("select[name='product_type']").val();
		// lay so luong
		var soluong = $("select[name='quantity']").val();
		$.ajax({
			type: "get",
			url: "{{url('/cart/additem')}}",
			data: {
				'loaisp': loaisp,
				'soluong': soluong
			},
			success: function(response) {
				// var jsonData = JSON.parse(response);
				// user is logged in successfully in the back-end
				// let's redirect
				// if (jsonData.success == "1") {
				// 	location.href = 'my_profile.php';
				// } else {
				// 	alert('Invalid Credentials!');
				// }
				alert(response.success);
			}
		});
	});
</Script>


@endsection