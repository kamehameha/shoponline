@extends('layouts.master')
@section('content')
<div class="inner-header">
		<div class="container">
			<div class="pull-left">
				<h6 class="inner-title">Sản phẩm</h6>
			</div>
			<div class="pull-right">
				<div class="beta-breadcrumb font-large">
					<a href="index.html">Home</a> / <span>Sản phẩm</span>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="container">
		<div id="content" class="space-top-none">
			<div class="main-content">
				<div class="space60">&nbsp;</div>
				<div class="row">
					<div class="col-sm-3">
						<ul class="aside-menu">
							<li><a href="#">Typography</a></li>
							<li><a href="#">Buttons</a></li>
							<li><a href="#">Dividers</a></li>
							<li><a href="#">Columns</a></li>
							<li><a href="#">Icon box</a></li>
							<li><a href="#">Notifications</a></li>
							<li><a href="#">Progress bars and Skill meter</a></li>
							<li><a href="#">Tabs</a></li>
							<li><a href="#">Testimonial</a></li>
							<li><a href="#">Video</a></li>
							<li><a href="#">Social icons</a></li>
							<li><a href="#">Carousel sliders</a></li>
							<li><a href="#">Custom List</a></li>
							<li><a href="#">Image frames &amp; gallery</a></li>
							<li><a href="#">Google Maps</a></li>
							<li><a href="#">Accordion and Toggles</a></li>
							<li class="is-active"><a href="#">Custom callout box</a></li>
							<li><a href="#">Page section</a></li>
							<li><a href="#">Mini callout box</a></li>
							<li><a href="#">Content box</a></li>
							<li><a href="#">Computer sliders</a></li>
							<li><a href="#">Pricing &amp; Data tables</a></li>
							<li><a href="#">Process Builders</a></li>
						</ul>
					</div>
					<div class="col-sm-9">
						<div class="beta-products-list">
							<h4>DANH SÁCH SẢN PHẨM</h4>
							<div class="beta-products-details">
								<div class="space10">&nbsp;</div>
								<div class="clearfix"></div>
							</div>

							<div class="row">
								@if (count($product_by_type) != 0)
									@FOREACH ($product_by_type as $sp)									
										<div class="col-sm-4">

											<div class="single-item" style="margin-bottom: 25px">

												<div class="single-item-header">
													<a href="{{Route('chitietsp',$sp->IDSP)}}"><img src="../{{$sp->HinhSP}} " alt="" width="200" height="200"></a>
												</div>

												<div class="single-item-body">
													<a class="single-item-title" href="{{Route('chitietsp',$sp->IDSP)}}">{{$sp->TenSP}}</a>
													<p class="single-item-price" style="margin: 5px 0px 5px 0px; color:orange">
														<span>{{number_format($sp->GiaNiemYet, 0, ',', '.')}}đ</span>
													</p>
												</div>

												<div class="single-item-caption">
													<!-- <form method="POST" action="{{url('cart')}}"> -->
														<!-- <a class="add-to-cart pull-left" href="#"><i class="fa fa-shopping-cart"></i></a> -->
														<!-- @csrf
														<input type="hidden" name="product_id" value="{{$sp->IDSP}}">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <button type="submit" class="add-to-cart pull-left">
                                        <i class="fa fa-shopping-cart"></i>
                                        
                                    </button>
													</form> -->
													<a class="beta-btn primary" href="#">Details <i class="fa fa-chevron-right"></i></a>
													<div class="clearfix"></div>
												</div>

											</div>

										</div>									
									@ENDFOREACH								
								@else	
									<p style="margin: 0px 0px 0px 30px">Không tìm thấy sản phẩm</p>
								@endif
							</div>
						</div> <!-- .beta-products-list -->

						<div class="space50">&nbsp;</div>
							
						</div> <!-- .beta-products-list -->
					</div>
				</div> <!-- end section with sidebar and main content -->


			</div> <!-- .main-content -->
		</div> <!-- #content -->
    </div> <!-- .container -->
@endsection