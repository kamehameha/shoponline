@extends('layouts.master')

@section('content')
<div class="container" style="padding-top: 15px; padding-bottom: 15px;">
	<div class="pull-left">
		<h6 class="inner-title">Shopping Cart</h6>
	</div>
	<div class="pull-right">
		<div class="beta-breadcrumb font-large">
			<a href="index.html">Home</a> / <span>Shopping Cart</span>
		</div>
	</div>
	<div class="clearfix"></div>
</div>
<div class="container">
	<div id="content">
		@if(isset($cart))
		@if(count($cart))
		<div class="table-responsive">
			<!-- Shop Products Table -->
			<table class="shop_table beta-shopping-cart-table" cellspacing="0">


				<thead>
					<tr>
						<th class="product-name">Product</th>
						<th class="product-price">Price</th>
						<!-- <th class="product-status">Status</th> -->
						<th class="product-quantity">Qty.</th>
						<th class="product-subtotal">Total</th>
						<th class="product-remove">Remove</th>
					</tr>
				</thead>
				<tbody>

					@foreach($cart as $item)
					<tr class="cart_item">
						<td class="product-name">
							<div class="media">
								<img class="pull-left" style="object-fit: contain;width: 100px;height: 100px;" src="{{ $item->options['img'] }}" alt="{{ $item->options['img'] }}">
								<div class="media-body">
									<p class="font-large table-title">{{ $item->name }}</p>
									<p class="table-option">IDLSP: {{ $item->id }}</p>
									<p class="table-option">Loại: {{ $item->options['tenloaisp'] }}</p>
									<!-- <a class="table-edit" href="#">Edit</a> -->
								</div>
							</div>
						</td>

						<td class="product-price">
							<span class="amount">{{ number_format($item->price)}} VNĐ</span>
						</td>

						<!-- <td class="product-status">
								In Stock
							</td> -->

						<td class="product-quantity">
							<select name="product-qty" id="product-qty">
								<option value="{{$item->qty}}">{{$item->qty}}</option>
								<!-- <option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option> -->
							</select>
						</td>

						<td class="product-subtotal">
							<span class="amount">{{ number_format($item->subtotal)}} VNĐ</span>
						</td>

						<td class="product-remove">
							<a href="/cart" class="remove" title="Remove this item" loaisp="{{$item->id}}"><i class="fa fa-trash-o"></i></a>
						</td>
					</tr>
					@endforeach
				</tbody>

				<tfoot>
					<tr>
						<!-- <td colspan="3" class="actions"  ></td>  -->
						<td colspan="5" class="actions" style="text-align: right; font-size: 1.3em;">

							<!-- <div class="coupon">
									<label for="coupon_code">Coupon</label> 
									<input type="text" name="coupon_code" value="" placeholder="Coupon code"> 
									<button type="submit" class="beta-btn primary" name="apply_coupon">Apply Coupon <i class="fa fa-chevron-right"></i></button>
								</div>
								 -->
							<!-- <button type="submit" class="beta-btn primary" name="update_cart">Update Cart <i class="fa fa-chevron-right"></i></button>  -->
							<button type="submit" class="beta-btn primary" style="padding:20px 20px 20px 20px; height: auto;" name="proceed">Tính tiền<i class="fa fa-chevron-right"></i></button>
						</td>
					</tr>
				</tfoot>

			</table>
			<!-- End of Shop Table Products -->
		</div>

		@if(isset($subcart))
		<!-- Cart Collaterals -->
		<div class="cart-collaterals">



			<div class="cart-totals pull-right">
				<div class="cart-totals-row">
					<h5 class="cart-total-title">Thanh toán</h5>
				</div>
				<div class="cart-totals-row"><span>Giỏ:</span> <span>{{ $subcart}} VND</span></div>
				<div class="cart-totals-row"><span>Giao hàng:</span> <span>Free</span></div>
				<div class="cart-totals-row"><span>Tổng:</span> <span>{{ $subcart}} VND</span></div>
			</div>

			<div class="clearfix"></div>
		</div>
		<!-- End of Cart Collaterals -->
		<div class="clearfix"></div>
		@endif

		<script>
			// var rock;
			$(document).ready(function() {
				$(".remove").click(function(e) {
					e.preventDefault();
					// rock = e;	
					// lay loai san pham
					var loaisp = e.currentTarget.getAttribute("loaisp");
					// lay so luong
					$.ajax({
						type: "get",
						url: "{{url('/cart/removeitem')}}",
						data: {
							'loaisp': loaisp
						},
						success: function(response) {

							$('a.remove[loaisp="'+loaisp+'"]').parent().parent().remove();
							
							alert(response.success);
							// if (response.success === "Add to cart successful"){
							// }
							location.reload();

						}
					});
				});
			});
		</script>

		@else
		<p>You have no items in the shopping cart</p>
		@endif
		@endif
	</div> <!-- #content -->
</div>

@endsection