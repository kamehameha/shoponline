<div class="bs-component sticky-top">
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <a class="navbar-brand" href="#">Shop Online</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="true" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="navbar-collapse collapse show" id="navbarColor01" style="">
            <ul class="navbar-nav ml-3">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Features</a>
                </li>

            </ul>

            <!-- dang nhap my-2 my-lg-0 -->

            <form class="form-inline mr-auto w-50 p-3">
                <input class="form-control w-75 p-3" type="text" placeholder="Search">
                <button class="btn btn-primary my-2 mx-sm-2" type="submit">Tìm kiếm</button>
            </form>

            @include('layouts.login')
        </div>
    </nav>
</div> 