<div id="header">

	<div class="header-top">
		<div class="container">
			<div class="pull-left auto-width-left">
				<ul class="top-menu menu-beta l-inline">
					<li><a href=""><i class="fa fa-home"></i> 90-92 Lê Thị Riêng, Bến Thành, Quận 1</a></li>
					<li><a href=""><i class="fa fa-phone"></i> 0163 296 7751</a></li>
				</ul>
			</div>
			<div class="pull-right auto-width-right">
				<ul class="top-details menu-beta l-inline">



					@include('layouts.login')

				</ul>
			</div>
			<div class="clearfix"></div>
		</div> <!-- .container -->
	</div> <!-- .header-top -->

	<!-- <div class="header-bottom" style="background-color: #0277b8;">
		<div class="container">
			<a class="visible-xs beta-menu-toggle pull-right" href="#"><span class='beta-menu-toggle-text'>Menu</span> <i class="fa fa-bars"></i></a>
			<div class="visible-xs clearfix"></div>
			<nav class="main-menu" style="display:inline-flex;">
				<ul class="l-inline ov">
					<li><a href="index.html">Trang chủ</a></li>
					<li><a href="#">Sản phẩm</a>
						<ul class="sub-menu">
							<li><a href="product_type.html">Sản phẩm 1</a></li>
							<li><a href="product_type.html">Sản phẩm 2</a></li>
							<li><a href="product_type.html">Sản phẩm 4</a></li>
						</ul>
					</li>
					<li><a href="about.html">Giới thiệu</a></li>
					<li><a href="contacts.html">Liên hệ</a></li>
					
				</ul>
				<a class="add-to-cart" href="#"><i class="fa fa-shopping-cart"></i></a>
				<div class="clearfix"></div>
			</nav>
		</div> 
	</div> -->
<nav class="navbar navbar-expand-lg navbar-dark  bg-dark " style="padding-left: 200px;padding-right: 200px;">
  <a class="navbar-brand" href="{{url('trang-chu')}}">Shoponline</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse " id="navbarSupportedContent" style="flex-grow: 0;">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Link</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Dropdown
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="#">Disabled</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0 " >
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" style="width: 400px;">
      <button class="btn btn-primary my-2 my-sm-0" type="submit" style="background: black; border-color: coral;">Search</button>
	</form>

	<a  href="{{url('/cart')}}" style="padding-left:200px;"><i class="fa fa-shopping-cart" style="font-size: 250%;"></i></a>
  </div>
</nav>

</div> <!-- #header -->