<style>
    #login {
        border-right: 1px solid #ddd;
        box-shadow: 1px 0 0 #fff;
    }

    #login-trigger,
    #signup-trigger,
    #signup a {
        display: inline-block;
        *display: inline;
        *zoom: 1;
        /* height: 25px;
		line-height: 25px; */
        font-weight: bold;
        padding: 0 8px;
        text-decoration: none;
        color: #444;
        text-shadow: 0 1px 0 #fff;
    }

    #signup a {
        border-radius: 0 3px 3px 0;
    }

    #login-trigger {
        border-radius: 3px 0 0 3px;
    }

    #login-trigger:hover,
    #login .active {
        background: #fff;
    }

    #login-content {
        display: none;
        position: absolute;
        top: 40px;
        right: 0;
        z-index: 999;
        background: #fff;
        background-image: linear-gradient(top, #fff, #eee);
        padding: 15px;
        box-shadow: 0 2px 2px -1px rgba(0, 0, 0, .9);
        border-radius: 3px 0 3px 3px;
    }

    li #login-content {
        right: 180px;
        width: 270px;
    }

    /*--------------------*/

    #inputs input {
        background: #f1f1f1;
        padding: 6px 5px;
        margin: 0 0 5px 0;
        width: 238px;
        border: 1px solid #ccc;
        border-radius: 3px;
        box-shadow: 0 1px 1px #ccc inset;
    }

    #inputs input:focus {
        background-color: #fff;
        border-color: #e8c291;
        outline: none;
        box-shadow: 0 0 0 1px #e8c291 inset;
    }

    /*--------------------*/

    #login #actions {
        margin: 10px 0 0 0;
    }

    #login #submit {
        background-color: #0277b8;
        background-image: linear-gradient(top, #e97171, #d14545);
        -moz-border-radius: 3px;
        -webkit-border-radius: 3px;
        border-radius: 3px;
        text-shadow: 0 1px 0 rgba(0, 0, 0, .5);
        box-shadow: 0 0 1px rgba(0, 0, 0, 0.3), 0 1px 0 rgba(255, 255, 255, 0.3) inset;
        border: 1px solid #7e1515;
        float: left;
        height: 30px;
        padding: 0;
        width: 100px;
        cursor: pointer;
        font: bold 14px Arial, Helvetica;
        color: #fff;
    }

    #login #submit:hover,
    #login #submit:focus {
        background-color: #6cc5f7;
        background-image: linear-gradient(top, #d14545, #e97171);
    }

    #login #submit:active {
        outline: none;
        box-shadow: 0 1px 4px rgba(0, 0, 0, 0.5) inset;
    }

    #login #submit::-moz-focus-inner {
        border: none;
    }

    #login label {
        float: right;
        line-height: 30px;
    }

    #login label input {
        position: relative;
        top: 2px;
        right: 2px;
    }
</style>
<!-- kiem tra dang nhap -->


@if (Auth::check())

<?php
// $user = session('user');
$user = Auth::user();

?>
@if (isset($user))

<!-- <div class="dropdown">
    <h6 style="color:white;" class="dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{$user->email}}</h6>


        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="#">Xem thông tin tài khoản</a>
            <a class="dropdown-item" href="#">Another action</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="{{url('logout')}}">đăng xuất</a>
        </div>
    </div> -->

<li><a href="#"><i class="fa fa-user"></i>{{$user->email}}</a></li>
<li><a href="{{url('logout')}}"><i class="fa fa-user"></i>Đăng xuất</a></li>

@endif

@if (isset($newparam))
@if ($newparam == 'signup_success')
<!-- đăng nhập thất bại show thông báo -->
<script>
    alert("Đăng ký thành công.");
</script>
@endif
@endif

@else

@if (isset($newparam))
@if ($newparam == 'login_error')
<!-- đăng nhập thất bại show thông báo -->
<script>
    alert("Bạn đã nhập sai thông tin đăng nhập, xin vui lòng nhập lại.");
</script>
@endif

@endif



<li id="login">
    <a id="login-trigger" href="#">
        Đăng nhập <span>▼</span>
    </a>
    <div id="login-content">
        <form action="{{route('login')}}" method="post" id="loginForm">
            <!-- action="{{route('login')}}" method="post" -->
            @csrf

            <!-- <form action="{{route('signup')}}" method="post">
                @csrf -->
            <fieldset id="inputs">
                <input id="username" type="email" name="Email" placeholder="Your email address" required>
                <input id="password" type="password" name="Password" placeholder="Password" required>
            </fieldset>
            <fieldset id="actions">

                <!-- <input type="submit" class="btn btn-primary" id="dangky" value="Đăng Ký"> -->
                <!-- </form> -->

                <input type="submit" class="btn btn-primary" id="dangnhap" value="Đăng Nhập Hoặc Đăng Ký" style=" background-color: #e06015;">

                <!-- <label><input type="checkbox" checked="checked">Nhớ mật khẩu</label> -->
            </fieldset>
        </form>
    </div>
</li>
<!-- <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.0.min.js"></script> -->


<script>
    $(document).ready(function() {
        $('#login-trigger').click(function() {
            $(this).next('#login-content').slideToggle();
            $(this).toggleClass('active');

            if ($(this).hasClass('active')) $(this).find('span').html('&#x25B2;')
            else $(this).find('span').html('&#x25BC;')
        });


    });

</script>
@endif