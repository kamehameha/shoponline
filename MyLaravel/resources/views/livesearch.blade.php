<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>live search</title>
    <!-- <link rel="stylesheet" href="{{asset('css\bootstrap-3.4.1\css\bootstrap.min.css')}}"> -->
    <!-- <link rel="stylesheet" href="{{asset('css\bootstrap-3.4.1\css\bootstrap-theme.min.css')}}"> -->
    <link rel="stylesheet" href="{{asset('css\united\bootstrap.min.css')}}">
    <style>
        table {
            table-layout: fixed;
            width: 1366px;
        }

        th,
        td {
            white-space: nowrap;
            width: 100px;
            overflow: hidden;
            text-overflow: ellipsis;
            word-wrap: break-word;
        }

        th,
        td:hover {
            overflow: visible;
        }
    </style>
</head>

<body>

    <!-- tim kiem -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <a class="navbar-brand" href="#">Navbar</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarColor01">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Features</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Pricing</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">About</a>
                </li>
            </ul>
            <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="text" placeholder="Search" id="search">
                <button class="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
            </form>
        </div>
    </nav>

    <!-- bang tim kiem -->
    <h3 align="center">Total Data: <span id="total_records"></span></h3>

    <table class="table table-hover">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Hình Ảnh</th>
                <th scope="col">Tên Sản Phẩm</th>
                <th scope="col">Mô tả</th>
                <th scope="col">Điểm Đánh Giá</th>
                <th scope="col">Giá bán</th>
            </tr>
        </thead>
        <tbody>

            <tr class="table-info">
                <th scope="row">Info</th>
                <td>Column content</td>
                <td>Column content</td>
                <td>Column content</td>
                <td>Column content</td>
                <td>Column content</td>
            </tr>

        </tbody>
    </table>
    <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{asset('css/bootstrap/js/bootstrap.min.js')}}"></script>
    <script>
        $(document).ready(function() {

            fetch_customer_data();

            function fetch_customer_data(query = '') {
                $.ajax({
                    url: "{{ route('livesearch.action') }}",
                    method: 'GET',
                    data: {
                        'query': query
                    },
                    dataType: 'json',
                    success: function(data) {
                        $('tbody').html(data.table_data);
                        $('#total_records').text(data.total_data);
                    }
                });
            }

            $(document).on('keyup', '#search', function() {
                var query = $(this).val();
                fetch_customer_data(query);
            });

        });
    </script>
</body>

</html> 