<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUseridToTaikhoan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('taikhoan', function (Blueprint $table) {
            //add column id of users to taikhoan
            $table->integer('user_id')->unsigned();
        });
        DB::table('taikhoan')
            ->update(['user_id' => 1]);

        Schema::table('taikhoan', function (Blueprint $table) {
            //add column id of users to taikhoan

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('taikhoan', function (Blueprint $table) {
            $table->dropColumn('user_id');
        });
    }
}
