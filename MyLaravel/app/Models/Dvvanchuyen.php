<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Mar 2019 16:05:31 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Dvvanchuyen
 * 
 * @property int $IDDVVC
 * @property string $TenDonViVC
 * @property int $IDDiaChi
 * @property string $MaDVVanChuyen
 * 
 * @property \App\Models\Diachi $diachi
 * @property \Illuminate\Database\Eloquent\Collection $phieudathangs
 * @property \Illuminate\Database\Eloquent\Collection $quydinhphivcs
 *
 * @package App\Models
 */
class Dvvanchuyen extends Eloquent
{
	protected $table = 'dvvanchuyen';
	protected $primaryKey = 'IDDVVC';
	public $timestamps = false;

	protected $casts = [
		'IDDiaChi' => 'int'
	];

	protected $fillable = [
		'TenDonViVC',
		'IDDiaChi',
		'MaDVVanChuyen'
	];

	public function diachi()
	{
		return $this->belongsTo(\App\Models\Diachi::class, 'IDDiaChi');
	}

	public function phieudathangs()
	{
		return $this->hasMany(\App\Models\Phieudathang::class, 'IDDVVC');
	}

	public function quydinhphivcs()
	{
		return $this->hasMany(\App\Models\Quydinhphivc::class, 'IDDVVC');
	}
}
