<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Mar 2019 16:05:31 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Phieudathang
 * 
 * @property int $IDPDH
 * @property int $IDTrangThaiPDH
 * @property string $NoiDungPDH
 * @property float $TongPhiVanChuyenPDH
 * @property float $TongTienHangPDH
 * @property float $TongTienPDH
 * @property int $IDKH_NguoiMua
 * @property int $IDDiaChi_NhanHang
 * @property int $IDDVVC
 * @property int $IDPTTT
 * @property string $LuuYCuaNguoiMua
 * @property \Carbon\Carbon $NgayDatHang
 * @property \Carbon\Carbon $NgayHoanTatPHD
 * @property \Carbon\Carbon $NgayYeuCauHuyDatHang
 * @property \Carbon\Carbon $NgayHuyDatHang
 * @property string $LyDoHuyPDH
 * 
 * @property \App\Models\Dvvanchuyen $dvvanchuyen
 * @property \App\Models\Diachi $diachi
 * @property \App\Models\Taikhoan $taikhoan
 * @property \App\Models\Phuongthucthanhtoan $phuongthucthanhtoan
 * @property \App\Models\Trangthaipdh $trangthaipdh
 * @property \Illuminate\Database\Eloquent\Collection $chitietdathangs
 * @property \Illuminate\Database\Eloquent\Collection $hoadons
 *
 * @package App\Models
 */
class Phieudathang extends Eloquent
{
	protected $table = 'phieudathang';
	protected $primaryKey = 'IDPDH';
	public $timestamps = false;

	protected $casts = [
		'IDTrangThaiPDH' => 'int',
		'TongPhiVanChuyenPDH' => 'float',
		'TongTienHangPDH' => 'float',
		'TongTienPDH' => 'float',
		'IDKH_NguoiMua' => 'int',
		'IDDiaChi_NhanHang' => 'int',
		'IDDVVC' => 'int',
		'IDPTTT' => 'int'
	];

	protected $dates = [
		'NgayDatHang',
		'NgayHoanTatPHD',
		'NgayYeuCauHuyDatHang',
		'NgayHuyDatHang'
	];

	protected $fillable = [
		'IDTrangThaiPDH',
		'NoiDungPDH',
		'TongPhiVanChuyenPDH',
		'TongTienHangPDH',
		'TongTienPDH',
		'IDKH_NguoiMua',
		'IDDiaChi_NhanHang',
		'IDDVVC',
		'IDPTTT',
		'LuuYCuaNguoiMua',
		'NgayDatHang',
		'NgayHoanTatPHD',
		'NgayYeuCauHuyDatHang',
		'NgayHuyDatHang',
		'LyDoHuyPDH'
	];

	public function dvvanchuyen()
	{
		return $this->belongsTo(\App\Models\Dvvanchuyen::class, 'IDDVVC');
	}

	public function diachi()
	{
		return $this->belongsTo(\App\Models\Diachi::class, 'IDDiaChi_NhanHang');
	}

	public function taikhoan()
	{
		return $this->belongsTo(\App\Models\Taikhoan::class, 'IDKH_NguoiMua');
	}

	public function phuongthucthanhtoan()
	{
		return $this->belongsTo(\App\Models\Phuongthucthanhtoan::class, 'IDPTTT');
	}

	public function trangthaipdh()
	{
		return $this->belongsTo(\App\Models\Trangthaipdh::class, 'IDTrangThaiPDH');
	}

	public function chitietdathangs()
	{
		return $this->hasMany(\App\Models\Chitietdathang::class, 'IDPDH');
	}

	public function hoadons()
	{
		return $this->hasMany(\App\Models\Hoadon::class, 'IDPDH');
	}
}
