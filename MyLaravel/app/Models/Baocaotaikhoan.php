<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Mar 2019 16:05:31 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Baocaotaikhoan
 * 
 * @property int $IDBCTK
 * @property string $NoiDungBC
 * @property int $MucDo
 * 
 * @property \Illuminate\Database\Eloquent\Collection $dsbaocaotks
 *
 * @package App\Models
 */
class Baocaotaikhoan extends Eloquent
{
	protected $table = 'baocaotaikhoan';
	protected $primaryKey = 'IDBCTK';
	public $timestamps = false;

	protected $casts = [
		'MucDo' => 'int'
	];

	protected $fillable = [
		'NoiDungBC',
		'MucDo'
	];

	public function dsbaocaotks()
	{
		return $this->hasMany(\App\Models\Dsbaocaotk::class, 'IDBCTK');
	}
}
