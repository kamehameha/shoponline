<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Mar 2019 16:05:31 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Vishopee
 * 
 * @property int $IDKH
 * @property float $SoDu
 * 
 * @property \App\Models\Taikhoan $taikhoan
 *
 * @package App\Models
 */
class Vishopee extends Eloquent
{
	protected $table = 'vishopee';
	protected $primaryKey = 'IDKH';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'IDKH' => 'int',
		'SoDu' => 'float'
	];

	protected $fillable = [
		'SoDu'
	];

	public function taikhoan()
	{
		return $this->belongsTo(\App\Models\Taikhoan::class, 'IDKH');
	}
}
