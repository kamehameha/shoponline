<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Mar 2019 16:05:31 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Trangthaihd
 * 
 * @property int $IDTrangThaiHD
 * @property string $MaTrangThaiHD
 * @property int $IDTrangThaiTiepTheo
 * 
 * @property \App\Models\Trangthaihd $trangthaihd
 * @property \Illuminate\Database\Eloquent\Collection $hoadons
 * @property \Illuminate\Database\Eloquent\Collection $trangthaihds
 *
 * @package App\Models
 */
class Trangthaihd extends Eloquent
{
	protected $table = 'trangthaihd';
	protected $primaryKey = 'IDTrangThaiHD';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'IDTrangThaiHD' => 'int',
		'IDTrangThaiTiepTheo' => 'int'
	];

	protected $fillable = [
		'MaTrangThaiHD',
		'IDTrangThaiTiepTheo'
	];

	public function trangthaihd()
	{
		return $this->belongsTo(\App\Models\Trangthaihd::class, 'IDTrangThaiTiepTheo');
	}

	public function hoadons()
	{
		return $this->hasMany(\App\Models\Hoadon::class, 'IDTrangThaiHD');
	}

	public function trangthaihds()
	{
		return $this->hasMany(\App\Models\Trangthaihd::class, 'IDTrangThaiTiepTheo');
	}
}
