<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Mar 2019 16:05:31 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Danhgium
 * 
 * @property int $IDDG
 * @property int $IDKH
 * @property int $IDSP
 * @property string $BinhLuan
 * @property float $Diem
 * @property \Carbon\Carbon $ThoiDiem
 * @property int $DaMuaSP
 * 
 * @property \App\Models\Taikhoan $taikhoan
 * @property \App\Models\Sanpham $sanpham
 *
 * @package App\Models
 */
class Danhgia extends Eloquent
{
	protected $table = 'Danhgia';
	protected $primaryKey = 'IDDG';
	public $timestamps = false;

	protected $casts = [
		'IDKH' => 'int',
		'IDSP' => 'int',
		'Diem' => 'float',
		'DaMuaSP' => 'int'
	];

	protected $dates = [
		'ThoiDiem'
	];

	protected $fillable = [
		'IDKH',
		'IDSP',
		'BinhLuan',
		'Diem',
		'ThoiDiem',
		'DaMuaSP'
	];

	public function taikhoan()
	{
		return $this->belongsTo(\App\Models\Taikhoan::class, 'IDKH');
	}

	public function sanpham()
	{
		return $this->belongsTo(\App\Models\Sanpham::class, 'IDSP');
	}
}
