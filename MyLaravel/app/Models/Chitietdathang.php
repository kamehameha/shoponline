<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Mar 2019 16:05:31 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Chitietdathang
 * 
 * @property int $IDPDH
 * @property int $IDLSP
 * @property int $SoLuong
 * @property float $DonGia
 * @property int $SoLuongDaGiao
 * @property \Carbon\Carbon $ThoiDiemCapNhatMoiNhat
 * 
 * @property \App\Models\Loaisp $loaisp
 * @property \App\Models\Phieudathang $phieudathang
 *
 * @package App\Models
 */
class Chitietdathang extends Eloquent
{
	protected $table = 'chitietdathang';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'IDPDH' => 'int',
		'IDLSP' => 'int',
		'SoLuong' => 'int',
		'DonGia' => 'float',
		'SoLuongDaGiao' => 'int'
	];

	protected $dates = [
		'ThoiDiemCapNhatMoiNhat'
	];

	protected $fillable = [
		'SoLuong',
		'DonGia',
		'SoLuongDaGiao',
		'ThoiDiemCapNhatMoiNhat'
	];

	public function loaisp()
	{
		return $this->belongsTo(\App\Models\Loaisp::class, 'IDLSP');
	}

	public function phieudathang()
	{
		return $this->belongsTo(\App\Models\Phieudathang::class, 'IDPDH');
	}
}
