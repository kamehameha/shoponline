<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Mar 2019 16:05:31 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Vishopeelsgd
 * 
 * @property int $IDLSGD
 * @property string $NoiDungGD
 * @property \Carbon\Carbon $ThoiDiem
 * @property float $SoTienGD
 * @property int $IDKH
 * @property int $IDHD
 * @property int $DaCongVaoSoDu
 * 
 * @property \App\Models\Hoadon $hoadon
 * @property \App\Models\Taikhoan $taikhoan
 *
 * @package App\Models
 */
class Vishopeelsgd extends Eloquent
{
	protected $table = 'vishopeelsgd';
	protected $primaryKey = 'IDLSGD';
	public $timestamps = false;

	protected $casts = [
		'SoTienGD' => 'float',
		'IDKH' => 'int',
		'IDHD' => 'int',
		'DaCongVaoSoDu' => 'int'
	];

	protected $dates = [
		'ThoiDiem'
	];

	protected $fillable = [
		'NoiDungGD',
		'ThoiDiem',
		'SoTienGD',
		'IDKH',
		'IDHD',
		'DaCongVaoSoDu'
	];

	public function hoadon()
	{
		return $this->belongsTo(\App\Models\Hoadon::class, 'IDHD');
	}

	public function taikhoan()
	{
		return $this->belongsTo(\App\Models\Taikhoan::class, 'IDKH');
	}
}
