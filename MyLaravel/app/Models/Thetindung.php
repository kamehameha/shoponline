<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Mar 2019 16:05:31 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Thetindung
 * 
 * @property int $IDTTD
 * @property string $HoTen
 * @property string $SoThe
 * @property int $ThangHetHan
 * @property int $NamHetHan
 * @property int $CVV
 * @property string $DiaChiDKThe
 * @property int $LuaChon
 * @property int $IDKH
 * 
 * @property \App\Models\Taikhoan $taikhoan
 *
 * @package App\Models
 */
class Thetindung extends Eloquent
{
	protected $table = 'thetindung';
	protected $primaryKey = 'IDTTD';
	public $timestamps = false;

	protected $casts = [
		'ThangHetHan' => 'int',
		'NamHetHan' => 'int',
		'CVV' => 'int',
		'LuaChon' => 'int',
		'IDKH' => 'int'
	];

	protected $fillable = [
		'HoTen',
		'SoThe',
		'ThangHetHan',
		'NamHetHan',
		'CVV',
		'DiaChiDKThe',
		'LuaChon',
		'IDKH'
	];

	public function taikhoan()
	{
		return $this->belongsTo(\App\Models\Taikhoan::class, 'IDKH');
	}
}
