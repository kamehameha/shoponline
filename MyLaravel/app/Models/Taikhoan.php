<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Mar 2019 16:05:31 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Taikhoan
 * 
 * @property int $IDKH
 * @property string $Email
 * @property string $SDT
 * @property string $TenDangNhap
 * @property string $TenShop
 * @property string $UrlShop
 * @property string $GioiTinh
 * @property \Carbon\Carbon $NgaySinh
 * @property string $Anhdaidien
 * @property string $MatKhau
 * @property int $Quyen
 * @property \Carbon\Carbon $NgayDK
 * @property int $TrangThai
 * 
 * @property \Illuminate\Database\Eloquent\Collection $danhgia
 * @property \Illuminate\Database\Eloquent\Collection $diachis
 * @property \Illuminate\Database\Eloquent\Collection $dsbaocaotks
 * @property \Illuminate\Database\Eloquent\Collection $hoadons
 * @property \Illuminate\Database\Eloquent\Collection $phieudathangs
 * @property \Illuminate\Database\Eloquent\Collection $sanphams
 * @property \Illuminate\Database\Eloquent\Collection $thetindungs
 * @property \Illuminate\Database\Eloquent\Collection $thongkedoanhthus
 * @property \Illuminate\Database\Eloquent\Collection $tknganhangs
 * @property \App\Models\Vishopee $vishopee
 * @property \Illuminate\Database\Eloquent\Collection $vishopeelsgds
 *
 * @package App\Models
 */
class Taikhoan extends Eloquent
{
	protected $table = 'taikhoan';
	protected $primaryKey = 'IDKH';
	public $timestamps = false;

	protected $casts = [
		'Quyen' => 'int',
		'TrangThai' => 'int',
		'user_id' => 'int'
	];

	protected $dates = [
		'NgaySinh',
		'NgayDK'
	];

	protected $fillable = [
		'Email',
		'SDT',
		'TenDangNhap',
		'TenShop',
		'UrlShop',
		'GioiTinh',
		'NgaySinh',
		'Anhdaidien',
		'MatKhau',
		'Quyen',
		'NgayDK',
		'TrangThai',
		'user_id'
	];

	public function danhgia()
	{
		return $this->hasMany(\App\Models\Danhgium::class, 'IDKH');
	}

	public function diachis()
	{
		return $this->hasMany(\App\Models\Diachi::class, 'IDKH');
	}

	public function dsbaocaotks()
	{
		return $this->hasMany(\App\Models\Dsbaocaotk::class, 'IDKH');
	}

	public function hoadons()
	{
		return $this->hasMany(\App\Models\Hoadon::class, 'IDKH_NguoiBan');
	}

	public function phieudathangs()
	{
		return $this->hasMany(\App\Models\Phieudathang::class, 'IDKH_NguoiMua');
	}

	public function sanphams()
	{
		return $this->hasMany(\App\Models\Sanpham::class, 'IDKH');
	}

	public function thetindungs()
	{
		return $this->hasMany(\App\Models\Thetindung::class, 'IDKH');
	}

	public function thongkedoanhthus()
	{
		return $this->hasMany(\App\Models\Thongkedoanhthu::class, 'IDKH');
	}

	public function tknganhangs()
	{
		return $this->hasMany(\App\Models\Tknganhang::class, 'IDKH');
	}

	public function vishopee()
	{
		return $this->hasOne(\App\Models\Vishopee::class, 'IDKH');
	}

	public function vishopeelsgds()
	{
		return $this->hasMany(\App\Models\Vishopeelsgd::class, 'IDKH');
	}
	
	
}
