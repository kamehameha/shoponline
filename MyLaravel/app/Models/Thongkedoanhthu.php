<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Mar 2019 16:05:31 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Thongkedoanhthu
 * 
 * @property int $IDTKDT
 * @property string $NoiDungThongKe
 * @property \Carbon\Carbon $ThoiGianBD
 * @property \Carbon\Carbon $ThoiGianKT
 * @property float $TongDoanhThu
 * @property int $SLHoaDonHoanTat
 * @property int $SLHoaDonBiHuy
 * @property int $TongSoHoaDon
 * @property int $IDKH
 * @property float $TongDoanhThuHangBan
 * 
 * @property \App\Models\Taikhoan $taikhoan
 *
 * @package App\Models
 */
class Thongkedoanhthu extends Eloquent
{
	protected $table = 'thongkedoanhthu';
	protected $primaryKey = 'IDTKDT';
	public $timestamps = false;

	protected $casts = [
		'TongDoanhThu' => 'float',
		'SLHoaDonHoanTat' => 'int',
		'SLHoaDonBiHuy' => 'int',
		'TongSoHoaDon' => 'int',
		'IDKH' => 'int',
		'TongDoanhThuHangBan' => 'float'
	];

	protected $dates = [
		'ThoiGianBD',
		'ThoiGianKT'
	];

	protected $fillable = [
		'NoiDungThongKe',
		'ThoiGianBD',
		'ThoiGianKT',
		'TongDoanhThu',
		'SLHoaDonHoanTat',
		'SLHoaDonBiHuy',
		'TongSoHoaDon',
		'IDKH',
		'TongDoanhThuHangBan'
	];

	public function taikhoan()
	{
		return $this->belongsTo(\App\Models\Taikhoan::class, 'IDKH');
	}
}
