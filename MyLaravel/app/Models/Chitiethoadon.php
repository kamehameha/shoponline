<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Mar 2019 16:05:31 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Chitiethoadon
 * 
 * @property int $IDHD
 * @property int $IDLSP
 * @property int $SoLuong
 * @property float $DonGia
 * @property \Carbon\Carbon $ThoiDiemCapNhatMoiNhat
 * @property int $SoLuongDaGiao
 * 
 * @property \App\Models\Loaisp $loaisp
 * @property \App\Models\Hoadon $hoadon
 *
 * @package App\Models
 */
class Chitiethoadon extends Eloquent
{
	protected $table = 'chitiethoadon';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'IDHD' => 'int',
		'IDLSP' => 'int',
		'SoLuong' => 'int',
		'DonGia' => 'float',
		'SoLuongDaGiao' => 'int'
	];

	protected $dates = [
		'ThoiDiemCapNhatMoiNhat'
	];

	protected $fillable = [
		'SoLuong',
		'DonGia',
		'ThoiDiemCapNhatMoiNhat',
		'SoLuongDaGiao'
	];

	public function loaisp()
	{
		return $this->belongsTo(\App\Models\Loaisp::class, 'IDLSP');
	}

	public function hoadon()
	{
		return $this->belongsTo(\App\Models\Hoadon::class, 'IDHD');
	}
}
