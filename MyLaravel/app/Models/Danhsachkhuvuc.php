<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Mar 2019 16:05:31 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Danhsachkhuvuc
 * 
 * @property string $MaKhuVuc
 * @property string $KhuVuc
 * 
 * @property \Illuminate\Database\Eloquent\Collection $diachis
 * @property \Illuminate\Database\Eloquent\Collection $quydinhphivcs
 *
 * @package App\Models
 */
class Danhsachkhuvuc extends Eloquent
{
	protected $table = 'danhsachkhuvuc';
	protected $primaryKey = 'MaKhuVuc';
	public $incrementing = false;
	public $timestamps = false;

	protected $fillable = [
		'KhuVuc'
	];

	public function diachis()
	{
		return $this->hasMany(\App\Models\Diachi::class, 'MaKhuVuc');
	}

	public function quydinhphivcs()
	{
		return $this->hasMany(\App\Models\Quydinhphivc::class, 'MaKhuVucNguoiMua');
	}
}
