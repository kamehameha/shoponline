<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Mar 2019 16:05:31 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Hoadon
 * 
 * @property int $IDHD
 * @property int $IDPDH
 * @property int $IDTrangThaiHD
 * @property string $NoiDungHD
 * @property float $PhiVanChuyenHD
 * @property float $TongTienHangHD
 * @property float $TongTienHD
 * @property int $IDKH_NguoiBan
 * @property int $IDDiaChi_LayHang
 * @property string $LuuYCuaNguoiBan
 * @property \Carbon\Carbon $NgayTaoHD
 * @property \Carbon\Carbon $NgayLayHang
 * @property \Carbon\Carbon $NgayNhanHang
 * @property \Carbon\Carbon $NgayChuyenTienVaoViShopee
 * @property \Carbon\Carbon $NgayThanhToanHoanTatHD
 * @property \Carbon\Carbon $NgayYeuCauHuyDon
 * @property \Carbon\Carbon $NgayHuyHoaDon
 * @property string $LyDoHuyHoaDon
 * 
 * @property \App\Models\Diachi $diachi
 * @property \App\Models\Taikhoan $taikhoan
 * @property \App\Models\Phieudathang $phieudathang
 * @property \App\Models\Trangthaihd $trangthaihd
 * @property \Illuminate\Database\Eloquent\Collection $chitiethoadons
 * @property \Illuminate\Database\Eloquent\Collection $vishopeelsgds
 *
 * @package App\Models
 */
class Hoadon extends Eloquent
{
	protected $table = 'hoadon';
	protected $primaryKey = 'IDHD';
	public $timestamps = false;

	protected $casts = [
		'IDPDH' => 'int',
		'IDTrangThaiHD' => 'int',
		'PhiVanChuyenHD' => 'float',
		'TongTienHangHD' => 'float',
		'TongTienHD' => 'float',
		'IDKH_NguoiBan' => 'int',
		'IDDiaChi_LayHang' => 'int'
	];

	protected $dates = [
		'NgayTaoHD',
		'NgayLayHang',
		'NgayNhanHang',
		'NgayChuyenTienVaoViShopee',
		'NgayThanhToanHoanTatHD',
		'NgayYeuCauHuyDon',
		'NgayHuyHoaDon'
	];

	protected $fillable = [
		'IDPDH',
		'IDTrangThaiHD',
		'NoiDungHD',
		'PhiVanChuyenHD',
		'TongTienHangHD',
		'TongTienHD',
		'IDKH_NguoiBan',
		'IDDiaChi_LayHang',
		'LuuYCuaNguoiBan',
		'NgayTaoHD',
		'NgayLayHang',
		'NgayNhanHang',
		'NgayChuyenTienVaoViShopee',
		'NgayThanhToanHoanTatHD',
		'NgayYeuCauHuyDon',
		'NgayHuyHoaDon',
		'LyDoHuyHoaDon'
	];

	public function diachi()
	{
		return $this->belongsTo(\App\Models\Diachi::class, 'IDDiaChi_LayHang');
	}

	public function taikhoan()
	{
		return $this->belongsTo(\App\Models\Taikhoan::class, 'IDKH_NguoiBan');
	}

	public function phieudathang()
	{
		return $this->belongsTo(\App\Models\Phieudathang::class, 'IDPDH');
	}

	public function trangthaihd()
	{
		return $this->belongsTo(\App\Models\Trangthaihd::class, 'IDTrangThaiHD');
	}

	public function chitiethoadons()
	{
		return $this->hasMany(\App\Models\Chitiethoadon::class, 'IDHD');
	}

	public function vishopeelsgds()
	{
		return $this->hasMany(\App\Models\Vishopeelsgd::class, 'IDHD');
	}
}
