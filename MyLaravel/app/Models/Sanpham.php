<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Mar 2019 16:05:31 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Sanpham
 * 
 * @property int $IDSP
 * @property string $TenSP
 * @property string $HinhSP
 * @property string $MoTaSP
 * @property int $DiemTB
 * @property \Carbon\Carbon $NgayDangSP
 * @property float $GiaNiemYet
 * @property int $IDDMSP
 * @property int $IDKH
 * @property int $CanNang
 * @property int $Dai
 * @property int $Rong
 * @property int $Cao
 * @property int $IDKH_LuuVet
 * 
 * @property \App\Models\Danhmucsp $danhmucsp
 * @property \App\Models\Taikhoan $taikhoan
 * @property \Illuminate\Database\Eloquent\Collection $danhgia
 * @property \Illuminate\Database\Eloquent\Collection $loaisps
 *
 * @package App\Models
 */
class Sanpham extends Eloquent
{
	protected $table = 'sanpham';
	protected $primaryKey = 'IDSP';
	public $timestamps = false;

	protected $casts = [
		'DiemTB' => 'float',
		'GiaNiemYet' => 'float',
		'IDDMSP' => 'int',
		'IDKH' => 'int',
		'CanNang' => 'int',
		'Dai' => 'int',
		'Rong' => 'int',
		'Cao' => 'int',
		'IDKH_LuuVet' => 'int'
	];

	protected $dates = [
		'NgayDangSP'
	];

	protected $fillable = [
		'TenSP',
		'HinhSP',
		'MoTaSP',
		'DiemTB',
		'NgayDangSP',
		'GiaNiemYet',
		'IDDMSP',
		'IDKH',
		'CanNang',
		'Dai',
		'Rong',
		'Cao',
		'IDKH_LuuVet'
	];

	public function danhmucsp()
	{
		return $this->belongsTo(\App\Models\Danhmucsp::class, 'IDDMSP');
	}

	public function taikhoan()
	{
		return $this->belongsTo(\App\Models\Taikhoan::class, 'IDKH');
	}

	public function danhgia()
	{
		return $this->hasMany(\App\Models\Danhgium::class, 'IDSP');
	}

	public function loaisps()
	{
		return $this->hasMany(\App\Models\Loaisp::class, 'IDSP');
	}
}
