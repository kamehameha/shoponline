<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Mar 2019 16:05:31 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Trangthaipdh
 * 
 * @property int $IDTrangThaiPDH
 * @property string $MaTrangThaiPDH
 * @property int $IDTrangThaiTiepTheo
 * 
 * @property \App\Models\Trangthaipdh $trangthaipdh
 * @property \Illuminate\Database\Eloquent\Collection $phieudathangs
 * @property \Illuminate\Database\Eloquent\Collection $trangthaipdhs
 *
 * @package App\Models
 */
class Trangthaipdh extends Eloquent
{
	protected $table = 'trangthaipdh';
	protected $primaryKey = 'IDTrangThaiPDH';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'IDTrangThaiPDH' => 'int',
		'IDTrangThaiTiepTheo' => 'int'
	];

	protected $fillable = [
		'MaTrangThaiPDH',
		'IDTrangThaiTiepTheo'
	];

	public function trangthaipdh()
	{
		return $this->belongsTo(\App\Models\Trangthaipdh::class, 'IDTrangThaiTiepTheo');
	}

	public function phieudathangs()
	{
		return $this->hasMany(\App\Models\Phieudathang::class, 'IDTrangThaiPDH');
	}

	public function trangthaipdhs()
	{
		return $this->hasMany(\App\Models\Trangthaipdh::class, 'IDTrangThaiTiepTheo');
	}
}
