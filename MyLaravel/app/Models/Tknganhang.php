<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Mar 2019 16:05:31 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Tknganhang
 * 
 * @property int $IDTheNH
 * @property string $HoTen
 * @property string $SoTaiKhoan
 * @property string $TenNganHang
 * @property string $KhuVuc
 * @property string $ChiNhanh
 * @property int $LuaChon
 * @property int $IDKH
 * 
 * @property \App\Models\Taikhoan $taikhoan
 *
 * @package App\Models
 */
class Tknganhang extends Eloquent
{
	protected $table = 'tknganhang';
	protected $primaryKey = 'IDTheNH';
	public $timestamps = false;

	protected $casts = [
		'LuaChon' => 'int',
		'IDKH' => 'int'
	];

	protected $fillable = [
		'HoTen',
		'SoTaiKhoan',
		'TenNganHang',
		'KhuVuc',
		'ChiNhanh',
		'LuaChon',
		'IDKH'
	];

	public function taikhoan()
	{
		return $this->belongsTo(\App\Models\Taikhoan::class, 'IDKH');
	}
}
