<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Mar 2019 16:05:31 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Loaisp
 * 
 * @property int $IDLSP
 * @property int $IDSP
 * @property string $TenLoaiSP
 * @property int $SLTreo
 * @property int $TonKho
 * @property float $DonGia
 * @property string $MoTaLoaiSP
 * @property int $IDSP_LuuVet
 * 
 * @property \App\Models\Sanpham $sanpham
 * @property \Illuminate\Database\Eloquent\Collection $chitietdathangs
 * @property \Illuminate\Database\Eloquent\Collection $chitiethoadons
 *
 * @package App\Models
 */
class Loaisp extends Eloquent
{
	protected $table = 'loaisp';
	protected $primaryKey = 'IDLSP';
	public $timestamps = false;

	protected $casts = [
		'IDSP' => 'int',
		'SLTreo' => 'int',
		'TonKho' => 'int',
		'DonGia' => 'float',
		'IDSP_LuuVet' => 'int'
	];

	protected $fillable = [
		'IDSP',
		'TenLoaiSP',
		'SLTreo',
		'TonKho',
		'DonGia',
		'MoTaLoaiSP',
		'IDSP_LuuVet'
	];

	public function sanpham()
	{
		return $this->belongsTo(\App\Models\Sanpham::class, 'IDSP');
	}

	public function chitietdathangs()
	{
		return $this->hasMany(\App\Models\Chitietdathang::class, 'IDLSP');
	}

	public function chitiethoadons()
	{
		return $this->hasMany(\App\Models\Chitiethoadon::class, 'IDLSP');
	}
}
