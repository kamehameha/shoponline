<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Mar 2019 16:05:31 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Phuongthucthanhtoan
 * 
 * @property int $IDPTTT
 * @property string $TenPhuongThuc
 * @property string $MaPTTT
 * 
 * @property \Illuminate\Database\Eloquent\Collection $phieudathangs
 *
 * @package App\Models
 */
class Phuongthucthanhtoan extends Eloquent
{
	protected $table = 'phuongthucthanhtoan';
	protected $primaryKey = 'IDPTTT';
	public $timestamps = false;

	protected $fillable = [
		'TenPhuongThuc',
		'MaPTTT'
	];

	public function phieudathangs()
	{
		return $this->hasMany(\App\Models\Phieudathang::class, 'IDPTTT');
	}
}
