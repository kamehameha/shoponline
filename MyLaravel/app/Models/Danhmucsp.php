<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Mar 2019 16:05:31 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Danhmucsp
 * 
 * @property int $IDDMSP
 * @property string $DanhMucCap1
 * @property string $DanhMucCap2
 * @property string $DanhMucCap3
 * @property string $ChiTietDanhMuc
 * 
 * @property \Illuminate\Database\Eloquent\Collection $sanphams
 *
 * @package App\Models
 */
class Danhmucsp extends Eloquent
{
	protected $table = 'danhmucsp';
	protected $primaryKey = 'IDDMSP';
	public $timestamps = false;

	protected $fillable = [
		'IDDMSP',
		'DanhMucCap1',
		'DanhMucCap2',
		'DanhMucCap3',
		'ChiTietDanhMuc'
	];

	public function sanphams()
	{
		return $this->hasMany(\App\Models\Sanpham::class, 'IDDMSP');
	}
}
