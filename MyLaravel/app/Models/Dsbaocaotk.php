<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Mar 2019 16:05:31 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Dsbaocaotk
 * 
 * @property int $IDDSBCTK
 * @property int $IDBCTK
 * @property int $IDKH
 * @property string $MoTaViPham
 * @property string $HinhThucXuLy
 * @property \Carbon\Carbon $NgayBaoCao
 * 
 * @property \App\Models\Taikhoan $taikhoan
 * @property \App\Models\Baocaotaikhoan $baocaotaikhoan
 *
 * @package App\Models
 */
class Dsbaocaotk extends Eloquent
{
	protected $table = 'dsbaocaotk';
	protected $primaryKey = 'IDDSBCTK';
	public $timestamps = false;

	protected $casts = [
		'IDBCTK' => 'int',
		'IDKH' => 'int'
	];

	protected $dates = [
		'NgayBaoCao'
	];

	protected $fillable = [
		'IDBCTK',
		'IDKH',
		'MoTaViPham',
		'HinhThucXuLy',
		'NgayBaoCao'
	];

	public function taikhoan()
	{
		return $this->belongsTo(\App\Models\Taikhoan::class, 'IDKH');
	}

	public function baocaotaikhoan()
	{
		return $this->belongsTo(\App\Models\Baocaotaikhoan::class, 'IDBCTK');
	}
}
