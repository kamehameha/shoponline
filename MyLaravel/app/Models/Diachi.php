<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Mar 2019 16:05:31 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Diachi
 * 
 * @property int $IDDiaChi
 * @property string $HoTen
 * @property string $SDT
 * @property string $MoTaDiaChi
 * @property string $LoiNhan
 * @property int $LuaChon
 * @property string $MaKhuVuc
 * @property int $IDKH
 * 
 * @property \App\Models\Taikhoan $taikhoan
 * @property \App\Models\Danhsachkhuvuc $danhsachkhuvuc
 * @property \Illuminate\Database\Eloquent\Collection $dvvanchuyens
 * @property \Illuminate\Database\Eloquent\Collection $hoadons
 * @property \Illuminate\Database\Eloquent\Collection $phieudathangs
 *
 * @package App\Models
 */
class Diachi extends Eloquent
{
	protected $table = 'diachi';
	protected $primaryKey = 'IDDiaChi';
	public $timestamps = false;

	protected $casts = [
		'LuaChon' => 'int',
		'IDKH' => 'int'
	];

	protected $fillable = [
		'HoTen',
		'SDT',
		'MoTaDiaChi',
		'LoiNhan',
		'LuaChon',
		'MaKhuVuc',
		'IDKH'
	];

	public function taikhoan()
	{
		return $this->belongsTo(\App\Models\Taikhoan::class, 'IDKH');
	}

	public function danhsachkhuvuc()
	{
		return $this->belongsTo(\App\Models\Danhsachkhuvuc::class, 'MaKhuVuc');
	}

	public function dvvanchuyens()
	{
		return $this->hasMany(\App\Models\Dvvanchuyen::class, 'IDDiaChi');
	}

	public function hoadons()
	{
		return $this->hasMany(\App\Models\Hoadon::class, 'IDDiaChi_LayHang');
	}

	public function phieudathangs()
	{
		return $this->hasMany(\App\Models\Phieudathang::class, 'IDDiaChi_NhanHang');
	}
}
