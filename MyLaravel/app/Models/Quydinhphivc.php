<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Mar 2019 16:05:31 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Quydinhphivc
 * 
 * @property int $IDQDPVC
 * @property int $IDDVVC
 * @property string $MaKhuVucNguoiBan
 * @property string $MaKhuVucNguoiMua
 * @property float $MucPhi
 * 
 * @property \App\Models\Dvvanchuyen $dvvanchuyen
 * @property \App\Models\Danhsachkhuvuc $danhsachkhuvuc
 *
 * @package App\Models
 */
class Quydinhphivc extends Eloquent
{
	protected $table = 'quydinhphivc';
	protected $primaryKey = 'IDQDPVC';
	public $timestamps = false;

	protected $casts = [
		'IDDVVC' => 'int',
		'MucPhi' => 'float'
	];

	protected $fillable = [
		'IDDVVC',
		'MaKhuVucNguoiBan',
		'MaKhuVucNguoiMua',
		'MucPhi'
	];

	public function dvvanchuyen()
	{
		return $this->belongsTo(\App\Models\Dvvanchuyen::class, 'IDDVVC');
	}

	public function danhsachkhuvuc()
	{
		return $this->belongsTo(\App\Models\Danhsachkhuvuc::class, 'MaKhuVucNguoiMua');
	}
}
