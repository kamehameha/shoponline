<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use DB;
use function GuzzleHttp\json_encode;

//Tạo controller
//php artisan make::controller MyController

class MyController extends Controller
{
    //test ham
    public function XinChao()
    {
        echo "CHào bâybe";
    }

    //test ham
    public function KhoaHoc($ten)
    {
        echo "Khoa học và " . $ten;
        // return redirect()->route('MyRoute1');
    }

    // request test

    public function GetURL(Request $request)
    {
        // return $request->path();
        // if ($request->isMethod('get'))
        //     echo $request->url();
        // else
        //     echo "Không phải phương thức post";
        if ($request->is('MyRequest*'))
            echo "có MyRequest";
        else
            echo "Không có MyRequest";
    }

    // postform 
    public function postForm(Request $request)
    {
        // echo "postForm: ". $request->HoTen;
        if ($request->has('tuoi'))
            echo "có tham số tuoi";
        else
            echo "Không có tgham số tuoi";
    }

    //tét côokie
    public function setCookie()
    {
        $response = new Response();
        $response->withCookie('Ten', 'Hoc Laravel - Hung', 1);
        return $response;
    }

    public function getCookie(Request $request)
    {
        return $request->cookie('Ten');
    }

    // upload file -> domain/uploadFile
    public function postFile(Request $request)
    {
        if ($request->hasFile('myFile')) {
            $file = $request->file('myFile');
            $file->move(
                'images', //duong dan noi chua file
                'savedFileUpload.jpg'
            );
            $filename = $file->getClientOriginalName('myFile');

            echo "da luu file." . $filename;
        } else {
            echo "chưa có file.";
        }
    }

    // tra ve du lieu JSON
    public function getJson()
    {
        return response()->json([
            'name' => 'Hung Nguyen',
            'tuoi' => 17
        ]);
    }

    // Test view: myView.php
    public function myView()
    {
        return view('view.subview');
    }

    //truyen tham so
    public function myViewThamSo($ten)
    {
        return view(
            'myView',
            ['ten' => $ten]
        );
    }

    // test AJAX: live search
    public function action(Request $request)
    {
        if ($request->ajax()) {
            $query = $request->get('query');
            if ($query != '') {
                // 
                $data = DB::table('sanpham')->where('TenSP', 'like', '%' . $query . '%')
                    ->orWhere('MoTaSP', 'like', '%' . $query . '%')
                    ->orWhere('HinhSP', 'like', '%' . $query . '%')
                    ->orWhere('GiaNiemYet', '=', $query)
                    ->orderBy('IDSP')
                    ->get();
            } else {
                $data = DB::table('sanpham')->orderBy('IDSP')->get();
            }
            $total_row = $data->count();
            $output = '';
            if ($total_row > 0) {
                foreach ($data as $row) {
                    $output .= '<tr class="table-info">
                        <td>' . $row->IDSP . '</td>
                        <td>' . $row->HinhSP . '</td>
                        <td>' . $row->TenSP . '</td>
                        <td>' . $row->MoTaSP . '</td>
                        <td>' . $row->DiemTB . '</td>
                        <td>' . $row->GiaNiemYet . '</td>
                    </tr>';
                }
            } else {
                    $output = '<tr class="table-info">
                <td align="center" colspan="6">No Data Found</td>
                </tr>';
                }
            $data = array(
                'table_data' => $output,
                'total_data' =>  $total_row
            );

            echo json_encode($data);
        }
    }
}
