<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\PageController;
use Illuminate\Support\Facades\Input;
use  App\User;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
class AuthController extends Controller
{
    //

    public function login(Request $request){
        
        $username =Input::get('Email'); // $request['Email'];
        $password =Input::get('Password'); // $request['Password'];

        if (Auth::attempt(['email'=>$username,'password'=>$password])){

            // session('user', Auth::user());

            return app('App\Http\Controllers\PageController')->getHomePage();
        }
        else
        {
            // session('login_error', 'sai thông tin đăng nhập');
            // $pagecontroller = new PageController;
            // return $pagecontroller->getHomePage();
            return app('App\Http\Controllers\PageController')->getHomePage($newparam='login_error'); //  example
        }
            // return view('pages.trangchu', ['error'=>'Đăng nhập thất bại.']);
    }

    public function logout(){
        Auth::logout();
        return app('App\Http\Controllers\PageController')->getHomePage();
    }

    public function signup(Request $request){
        
        $username = Input::get('Email');
        $password = Input::get('Password');
        if (isset($username) && isset($password )){

            if (Auth::attempt(['email'=>$username,'password'=>$password])){

                // session('user', Auth::user());
    
                return app('App\Http\Controllers\PageController')->getHomePage();
            }
            else 
            {
                // dang ky
                
                
                // DB::table('users')->insert([
        
                //     'name'=>'Anonymous',
                //     'email'=>$username,
                //     'password'=>bcrypt( $password )
                
                // ]);
                $newuser = new User;
                $newuser->name = 'Anonymous';
                $newuser->email = $username;
                $newuser->password = bcrypt( $password );
                $newuser->save();

                if (Auth::attempt(['email'=>$username,'password'=>$password])){

                    $mytime = Carbon::now();
                    
                    $user = Auth::user();
                    
                    
                    DB::table('taikhoan')->insert([
            
                        'Email' => $username, 
                        'SDT' => '', 
                        'TenDangNhap' => '', 
                        'TenShop' => $username, 
                        'UrlShop' => $username, 
                        'GioiTinh' => 'Nam', 
                        'NgaySinh' => '2000-01-01', 
                        'Anhdaidien' => '', 
                        'MatKhau' => '', 
                        'Quyen' => 0, // thuong 0 ; admin 1
                        'NgayDK' => $mytime->toDateTimeString(), 
                        'TrangThai' => 0,   // thuong 0 ; banned 1
                        'user_id' => $user->id,
                    ]);

                    return app('App\Http\Controllers\PageController')->getHomePage($newparam='signup_success'); //  example
                
                }
                    
            }
        }
        echo 'Đăng kí thất bại nha boy.';
    }
}
