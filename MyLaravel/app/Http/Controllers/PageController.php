<?php

namespace App\Http\Controllers;
use App\Models\KhuyenMai;
use App\Models\Sanpham;
use App\Models\Danhmucsp;
use App\Models\Danhgia;
use Illuminate\Http\Request;
use App\Models\Loaisp;

class PageController extends Controller
{
    public function getHomePage($newparam='none'){
        $product_type1 = Danhmucsp::select('IDDMSP','DanhMucCap1')->groupBy('DanhMucCap1','IDDMSP')->get();
        $danhmuc_flag = $product_type1[0]['DanhMucCap1'];
        $i = 0;
        foreach($product_type1 as $key) {
            if ($danhmuc_flag != $key['DanhMucCap1'] ){
                $danhmuc_flag = $key['DanhMucCap1'];
            }
            else
            {
                if ($i != 0)
                    unset($product_type1[$i]);
                
            }
            $i++;
        }
        return view('pages.trangchu',compact('product_type1','newparam'));
    }

    public function getProductList(Request $req){      
        $product_type_name = Danhmucsp::where('IDDMSP', $req->id_type1)->first();
 
        $all_product_type_id = Danhmucsp::where('DanhMucCap1', $product_type_name->DanhMucCap1)->get();
        $id_products = array();
        foreach ($all_product_type_id as $product_type_id) {
            $id_products[] = $product_type_id->IDDMSP;
        }
        $product_by_type = Sanpham::whereIn('IDDMSP', $id_products)->get();
        return view('pages.danhsachsp', compact('product_by_type'));
    }

    public function getProductDetail(Request $req){
        $sanpham = Sanpham::where('IDSP',$req->id_product)->first();
        $danhgiasp = Danhgia::where('IDSP',$sanpham->IDSP)->get();
        // lay danh sach loai san pham
        $dsloaisp = Loaisp::where('IDSP', $sanpham->IDSP)->get();
        return view('pages.chitietsp', compact('sanpham', 'danhgiasp','dsloaisp'));
    }
}
