<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Redirect;

use App\Models\Sanpham;
use Cart;
use App\Models\Loaisp;

class CartController extends Controller
{
    
    public function cart()
    {
    //    if (Request::isMethod('post')) {
    //         $this->data['title'] = 'Giỏ hàng của bạn';
    //         $product_id = Request::get('product_id');
    //         $product = Sanpham::where('IDSP',$product_id)->first();
    //         $cartInfo = [
    //             'id' => $product_id,
    //             'name' => $product->TenSP,
    //             'price' => $product->GiaNiemYet,
    //             'qty' => '1', //fix lai
    //             'options' => ['img' => $product->HinhSP],
    //         ];
    //         Cart::add($cartInfo);
            
    //     }
        $cart = Cart::content();
        $this->data['cart'] = $cart;
        
        // tinh tong tien
        
        $subcart = Cart::subtotal(0); // non decimal
        $this->data['subcart'] = $subcart;

        // tinh tien ship -> free

        return view('layouts.cart',  $this->data);
        // return view('layouts.cart',  compact('cart'));
       
    }
    public function removeitem()
    {
        $result='not successful';
        $loaisp='';
        
        if (Request::isMethod('get')) {

            $loaisp = request('loaisp', $default = null);
            
            if (isset($loaisp)){
                $cart = Cart::content();

                foreach($cart as $item){
                    if ($item->id === intval($loaisp)) {
                        Cart::remove($item->rowId);
                        $result = 'Remove item successful';
                        // break;
                    }
                }
            }
        }
        
        return response()->json(['success' => $result]);
    }
    public function addtocart(Request $rq)
    {
        $result='not successful';
        $loaisp='';
        $soluong = '';
        if (Request::isMethod('get')) {
            
            $loaisp = request('loaisp', $default = null);
            $soluong = request('soluong', $default = null);
            if (isset($loaisp) && isset($soluong)){
                
                $product_type = Loaisp::where('IDLSP', $loaisp)->first();
                
                if (isset($product_type)){
                    if (intval($product_type->SLTreo) + intval($soluong) < intval($product_type->TonKho))
                    {
                        $product = Sanpham::where('IDSP', $product_type->IDSP)->first();
                        $cartInfo = [
                            'id' => $product_type->IDLSP,
                            'name' => $product->TenSP,
                            'price' => $product_type->DonGia,
                            'qty' => $soluong, //fix lai
                            'options' => [
                                'img' => $product->HinhSP,
                                'tenloaisp' => $product_type->TenLoaiSP
                            ],
                        ];
                        Cart::add($cartInfo);
                        
                        $result = 'Add to cart successful';
                    }
                }


            }
        }
        
        return response()->json(['success' => $result]);
    }
}