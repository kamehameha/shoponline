<?php

namespace App\Http\Middleware;

use Closure;

class MyMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // kiem tra middle
        // http://localhost:8080/laravel/shoponline/MyLaravel/public/middleware/codiem?diem=4
        if ($request->has('diem'))
            return $next($request);
        else
            return redirect()->route('middleware/loi');
    }
}
